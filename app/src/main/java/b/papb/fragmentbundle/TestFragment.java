package b.papb.fragmentbundle;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link TestFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link TestFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class TestFragment extends Fragment {

    private static final String ARG_NAMA = "nama";

    private String namaParams;

    private EditText nama;

    private OnFragmentInteractionListener mListener;

    public TestFragment() {
        // Required empty public constructor
    }

    public static TestFragment newInstance(String namaParams) {
        Log.e("### Fragment", "Membuat Fragment baru dari metode `newInstance`" );
        TestFragment fragment = new TestFragment();

        Log.e("### Fragment", "Membuat bundle baru untuk mengirimkan data dari activity ke fragment");
        Bundle args = new Bundle();

        Log.e("### Fragment", "Memasukkan data : " + namaParams);
        args.putString(ARG_NAMA, namaParams);

        Log.e("### Fragment", "Menempelkan bundle ke fragment");
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Log.e("### Fragment", "Memeriksa apakah ada bundle yang ditempelkan ke fragment");
        if (getArguments() != null) {
            Log.e("### Fragment", "Ada bundle yang ditempelkan!! ambil datanya");
            namaParams = getArguments().getString(ARG_NAMA);
            Log.e("### Fragment", "Data yang diambil adalah : " + namaParams);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        Log.e("### Fragment", "Menyiapkan View untuk ditempelkan ke Fragment");
        return inflater.inflate(R.layout.fragment_test, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Log.e("### Fragment", "View berhasil dibuat dan ditempelkan ke Fragment");
        Log.e("### Fragment", "Inisialisasi View menggunakan findViewById()");

        nama = view.findViewById(R.id.nama);
        nama.setText(namaParams);

        view.findViewById(R.id.button)
                .setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("### Fragment", "Tombol pada Fragmen diklik");
                onButtonPressed(nama.getText().toString());
            }
        });
    }

    public void onButtonPressed(String string) {
        Log.e("### Fragment", "Memeriksa apakah ada listener/interface yang aktif....");
        if (mListener != null) {
            Log.e("### Fragment", "Ada interface yang aktif, kirimkan data ke interface");
            mListener.onFragmentInteraction(string);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
            Log.e("### Fragment", "Fragment berhasil ditempelkan ke Activity");
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {

        void onFragmentInteraction(String string);
    }
}
