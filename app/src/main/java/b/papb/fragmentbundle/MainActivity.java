package b.papb.fragmentbundle;

import android.net.Uri;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity
        implements TestFragment.OnFragmentInteractionListener {

    TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textView = findViewById(R.id.text);

        TestFragment fragment = TestFragment.newInstance("Ridwan");
        Log.e("### ACTIVITY", "Fragment baru berhasil dibuat");

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.content, fragment)
                .commit();
    }

    @Override
    public void onFragmentInteraction(String string) {
        Log.e("### ACTIVITY", "Data dikirim dari Fragment: " + string);
        textView.setText(string);
        finish();
    }
}
